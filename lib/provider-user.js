'use strict';
var _ = require('underscore');
var helper = require('./helper');
var md5 = require('MD5');
var ObjectId = require('mongolian').ObjectId;

var Provider = module.exports = function (db) {
	this.collection = db.collection('users');
};

Provider.prototype.find = function (query, callback) {
	this.collection.find(query).toArray(function (err, array) {
		callback(err, array);
	});
};

Provider.prototype.findGroupedByDate = function (query, callback) {
	this.find(query, function (err, array) {
		var grouped = helper.groupByDate(array);
		callback(err, grouped, array);
	});
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.get = function (application, username, password, callback) {
	this.find({ 'application' : application, 'username' : username, 'password' : md5(password) }, callback);
};

Provider.prototype.getByUsername = function (application, username, callback) {
	this.find({ 'application' : application, 'username' : username }, callback);
};

Provider.prototype.getByApplication = function (application, callback) {
	this.find({ 'application' : application }, callback);
};

Provider.prototype.getByApplicationFiltered = function (application, startdate, enddate, user, code, callback) {
	var cbAppend = _.bind(function (query) {
		this.find(this.convertQueryUserToID(query), callback);
	}, this);

	this.appendFiltersToQuery(
		{ 'application' : application },
		startdate,
		enddate,
		user,
		code,
		cbAppend
	);
};

Provider.prototype.getByApplicationAndEmail = function (application, email, callback) {
	this.find({ 'application' : application, 'email' : email }, callback);
};

Provider.prototype.getByApplicationAndCode = function (application, code, callback) {
	this.find({ 'application' : application, 'code' : code }, callback);
};

Provider.prototype.getByCode = function (code, callback) {
	this.find({ 'code' : code }, callback);
};

Provider.prototype.add = function (application, username, password, firstname, lastname, email, code, data, session, callback) {
	this.exists(application, username, _.bind(function (exists) {
		if (exists) {
			callback({ 'success': false, 'error' : 'user with username ' + username + ' already exists.' }, null);
		} else {
			this.collection.insert({
				'application' : application,
				'username' : username,
				'password' : md5(password),
				'name': { 'first' : firstname, 'last' : lastname },
				'email' : email,
				'code' : code,
				'data' : data,
				'date' : new Date(),
				'session' : session
			}, callback);
		}
	}, this));
};

Provider.prototype.update = function (application, username, password, firstname, lastname, email, code, data, callback) {
	this.exists(application, username, _.bind(function (exists) {
		if (exists) {
			this.collection.update(
				{ 'application' : application, 'username' : username },
				{ '$set': {
					'name': { 'first' : firstname, 'last' : lastname },
					'email' : email,
					'code' : code,
					'data' : data
				} },
				callback
			);
		} else {
			callback({ 'success': false, 'error' : 'user with username ' + username + ' does not exist.' }, null);
		}
	}, this));
};

Provider.prototype.exists = function (application, username, callback) {
	this.getByUsername(application, username, function (err, array) {
		callback(array.length > 0);
	});
};

Provider.prototype.existsByEmail = function (application, email, callback) {
	this.getByApplicationAndEmail(application, email, function (err, array) {
		callback(array.length > 0);
	});
};

Provider.prototype.validate = function (application, username, password, callback) {
	this.get(application, username, password, function (err, array) {
		callback(array.length > 0, array);
	});
};

Provider.prototype.getSystemInfoByApplication = function (application, callback) {
	this.collection.find(
		{
			'application' : application,
			'systeminfo' : { '$exists' : true }
		},
		{ 'systeminfo' : 1 }
	).toArray(callback);
};

Provider.prototype.getSystemInfoByApplicationFiltered = function (application, user, code, callback) {
	this.appendFiltersToQuery(
		{ 'application' : application, 'systeminfo' : { '$exists' : true } },
		null,
		null,
		user,
		code,
		_.bind(function (query) {
			this.collection.find(this.convertQueryUserToID(query), { 'systeminfo' : 1 }).toArray(callback);
		}, this)
	);
};

Provider.prototype.setSystemInfo = function (application, username, systeminfo, callback) {
	this.exists(application, username, _.bind(function (exists) {
		if (exists) {
			this.collection.update(
				{ 'application' : application, 'username' : username },
				{ '$set': { 'systeminfo' : systeminfo } },
				callback
			);
		} else {
			callback({ 'success': false, 'error' : 'user with username ' + username + ' does not exist.' }, null);
		}
	}, this));
};

Provider.prototype.setTRMOpt = function (application, username, opt, callback) {
	this.exists(application, username, _.bind(function (exists) {
		if (exists) {
			this.collection.update(
				{ 'application' : application, 'username' : username },
				{ '$set': { 'trm.opt' : opt } },
				callback
			);
		} else {
			callback({ 'success': false, 'error' : 'user with username ' + username + ' does not exist.' }, null);
		}
	}, this));
};

Provider.prototype.getCountByTRMOpt = function (application, opt, callback) {
	this.collection.count({ 'application' : application, 'trm.opt' : opt }, callback);
};

Provider.prototype.getCountByTRMOptFiltered = function (application, opt, startdate, enddate, user, code, callback) {
	this.appendFiltersToQuery(
		{ 'application' : application, 'trm.opt' : opt },
		startdate,
		enddate,
		user,
		code,
		_.bind(function (query) {
			this.collection.count(this.convertQueryUserToID(query), callback);
		}, this)
	);
};

Provider.prototype.convertQueryUserToID = function (query) {
	// Adjust query to move user property into _id
	if (query.user) {
		if (query.user.$in) {
			query.user.$in = _.map(query.user.$in, function (value) {
				return new ObjectId(value);
			});

			query._id = query.user;
		} else {
			query._id = new ObjectId(query.user.toString());
		}

		delete query.user;
	}
	return query;
};

Provider.prototype.getCountByApplicationAndCode = function (application, code, callback) {
	this.collection.count({ 'application' : application, 'code' : code }, callback);
};

Provider.prototype.appendFiltersToQuery = function (query, startdate, enddate, user, code, callback) {
	var q, cbCode, cbAppend;

	cbCode = _.bind(function (err, array) {
		var userids = _.map(array, function (value) { return value._id.toString(); });
		q.user = { '$in' : userids };
		callback(q);
	}, this);

	cbAppend = _.bind(function (query) {
		q = query;

		if (!user && code) {
			this.getByCode(code, cbCode);
		} else {
			callback(q);
		}
	}, this);

	helper.appendFiltersToQuery(query, startdate, enddate, user, cbAppend);
};