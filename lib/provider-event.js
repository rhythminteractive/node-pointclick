'use strict';
var _ = require('underscore');
var helper = require('./helper');
var UserProvider = require('./provider-user');

var Provider = module.exports = function (db) {
	this.collection = db.collection('events');
	this.userprovider = new UserProvider(db);
};

Provider.prototype.find = function (query, callback) {
	this.collection.find(query).toArray(function (err, array) {
		callback(err, array);
	});
};

Provider.prototype.findGroupedByDate = function (query, callback) {
	this.find(query, function (err, array) {
		var grouped = helper.groupByDate(array);

		callback(err, grouped, array);
	});
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.get = function (application, callback) {
	this.find({ 'application' : application }, callback);
};

Provider.prototype.getByType = function (application, type, callback) {
	this.find({ 'application' : application, 'type' : type }, callback);
};

Provider.prototype.getFiltered = function (application, startdate, enddate, user, code, callback) {
	this.userprovider.appendFiltersToQuery(
		{ 'application' : application },
		startdate,
		enddate,
		user,
		code,
		_.bind(function (query) {
			this.find(query, callback);
		}, this)
	);
};

Provider.prototype.getByTypeFiltered = function (application, type, startdate, enddate, user, code, callback) {
	this.userprovider.appendFiltersToQuery(
		{ 'application' : application, 'type' : type },
		startdate,
		enddate,
		user,
		code,
		_.bind(function (query) {
			this.find(query, callback);
		}, this)
	);
};

Provider.prototype.getByScopeAndType = function (application, scope, type, callback) {
	this.find({ 'application' : application, 'scope' : scope, 'type' : type }, callback);
};

Provider.prototype.getByScopeTypeAndPage = function (application, scope, type, page, callback) {
	this.find({ 'application' : application, 'scope' : scope, 'type' : type, 'page' : page }, callback);
};

Provider.prototype.add = function (application, user, session, scope, type, page, name, data, callback) {
	this.collection.insert({
		'application' : application,
		'user' : user,
		'session' : session,
		'scope' : scope,
		'type' : type,
		'page' : page,
		'name' : name,
		'data' : data,
		'date' : new Date()
	}, callback);
};

Provider.prototype.getByScopeAndType_groupByDate = function (application, scope, type, callback) {
	this.findGroupedByDate({ 'application' : application, 'scope' : scope, 'type' : type }, callback);
};

Provider.prototype.getByScopeAndType_groupByDateFiltered = function (application, scope, type, startdate, enddate, user, code, callback) {
	this.userprovider.appendFiltersToQuery(
		{ 'application' : application, 'scope' : scope, 'type' : type },
		startdate,
		enddate,
		user,
		code,
		_.bind(function (query) {
			this.findGroupedByDate(query, callback);
		}, this)
	);
};

Provider.prototype.getCountAndAverageByScopeAndType = function (application, scope, type, callback) {
	this.findGroupedByDate({ 'application' : application, 'scope' : scope, 'type' : type }, function (err, grouped, array) {
		var average = array.length / _.size(grouped);
		callback(err, {'average' : average, 'count' : array.length });
	});
};

Provider.prototype.getCountAndAverageByScopeAndTypeFiltered = function (application, scope, type, startdate, enddate, user, code, callback) {
	var count, average, cbGroup, cbAppend;

	cbGroup = _.bind(function (err, grouped, array) {
		count = array.length;
		average = (count / _.size(grouped)) || 0;
		callback(err, {'average' : average, 'count' : count });
	}, this);

	cbAppend = _.bind(function (query) {
		this.findGroupedByDate(query, cbGroup);
	}, this);

	this.userprovider.appendFiltersToQuery(
		{ 'application' : application, 'scope' : scope, 'type' : type },
		startdate,
		enddate,
		user,
		code,
		cbAppend
	);
};

Provider.prototype.getCountByScopeAndType = function (application, scope, type, callback) {
	return this.collection.count({ 'application' : application, 'scope' : scope, 'type' : type }, callback);
};

Provider.prototype.getAveragePerDayByScopeAndType = function (application, scope, type, callback) {
	this.find({ 'application' : application, 'scope' : scope, 'type' : type }, function (err, array) {
		var average = array.length / _.chain(array).groupBy(function (value) {
			return value.date.toYMD();
		}).size().value();

		callback(err, average);
	});
};