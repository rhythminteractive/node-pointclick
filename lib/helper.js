'use strict';
var _ = require('underscore');
var exports = module.exports;
var dateutils = require('date-utils');

Number.prototype.toDiskSize = function () {
	var units = ['b', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
		i = units.length - 1,
		size;

	while (i >= 0) {
		size = Math.pow(1024, i);
		if (this >= size) {
			return (this / size).toFixed(1) + ' ' + units[i];
		}
		i--;
	}

	return this + ' ' + units[0];
};

exports.getClientIp = function (req) {
	var ipAddress,
		forwardedIps,
		forwardedIpsStr = req.header('x-forwarded-for'); // Amazon EC2 / Heroku workaround to get real client IP

	if (forwardedIpsStr) {
		// 'x-forwarded-for' header may return multiple IP addresses in the format: "client IP, proxy 1 IP, proxy 2 IP" so take the the first one
		forwardedIps = forwardedIpsStr.split(',');
		ipAddress = forwardedIps[0];
	}

	ipAddress = ipAddress || req.connection.remoteAddress; // Ensure getting client IP address still works in development environment

	return ipAddress;
};

exports.allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
};

exports.expireHeaders = function (req, res, next) {
    res.setHeader('Cache-Control', 'public, max-age=345600'); // 4 days
    res.setHeader('Expires', new Date(Date.now() + 345600000).toUTCString());
    next();
};

exports.csvHeaders = function (req, res, next) {
	res.contentType('text/csv');
    next();
};

exports.groupByDate = function (array) {
	return _.groupBy(array, function (value) { return value.date.toYMD(); });
};

exports.parseFilters = function (query_filter) {
	var filters = {};

	if (query_filter) {
		_.each(JSON.parse(query_filter), function (value, key) {
			filters[value.property] = value.value;
		});
	}

	if (filters.startdate) {
		filters.startdate = new Date(Date.parse(filters.startdate));
	}

	if (filters.enddate) {
		filters.enddate = new Date(Date.parse(filters.enddate));
	}

	return filters;
};

exports.appendFiltersToQuery = function (query, startdate, enddate, user, callback) {
	query = query || {};

	if (user) {
		query.user = user;
	}

	if (startdate || enddate) {
		query.date = {};

		if (startdate) {
			query.date.$gte = startdate;
		}

		if (enddate) {
			query.date.$lte = enddate;
		}
	}

	if (callback) {
		callback(query);
	}
};

exports.isLoggedIn = function (req, res, next) {
	if (req.session.loggedin) {
		next();
	} else {
		res.json({'success': false, 'error' : 'you must be logged in to perform this action' });
	}
};