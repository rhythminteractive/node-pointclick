'use strict';
var _ = require('underscore');

var Provider = module.exports = function (db) {
	this.collection = db.collection('applications');
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.get = function (name, callback) {
	this.collection.find({ 'name' : name }).toArray(callback);
};

Provider.prototype.add = function (name, description, distribution, data, callback) {
	this.exists(name, _.bind(function (exists) {
		if (exists) {
			callback({ 'success': false, 'error' : 'application with name ' + name + ' already exists.' }, null);
		} else {
			this.collection.insert({
				'name' : name,
				'description' : description,
				'distribution' : distribution,
				'data' : data,
				'date' : new Date()
			}, callback);
		}
	}, this));
};

Provider.prototype.exists = function (name, callback) {
	this.get(name, function (err, array) {
		callback(array.length);
	});
};

Provider.prototype.getUI = function (name, componentID, callback) {
	var query = { 'name' : name };
	query['ui.' + componentID] = {'$exists' : true};

	this.collection.find(query).toArray(function (err, array) {
		if (array.length) {
			callback({ 'success' : true, 'component' : array[0].ui[componentID] });
		} else {
			callback({ 'success' : false, 'error' : 'component with id ' + componentID + ' does not exist.' });
		}
	});
};