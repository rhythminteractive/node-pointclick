'use strict';
var md5 = require('MD5');
var _ = require('underscore');

var Provider = module.exports = function (db) {
	this.collection = db.collection('admins');
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.get = function (username, password, callback) {
	this.collection.find({'username' : username, 'password' : md5(password) }).toArray(callback);
};

Provider.prototype.add = function (application, username, password, data, session, callback) {
	this.exists(username, _.bind(function (exists) {
		if (exists) {
			callback({ 'success': false, 'error' : 'admin with username ' + username + ' already exists.' }, null);
		} else {
			this.collection.insert({
				'application' : application,
				'username' : username,
				'password' : md5(password),
				'data' : data
			}, callback);
		}
	}, this));
};

Provider.prototype.exists = function (username, callback) {
	this.getByUsername(username, function (err, array) {
		callback(array.length);
	});
};