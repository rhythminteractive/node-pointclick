'use strict';

var Provider = module.exports = function (db) {
	this.collection = db.collection('surveys');
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.add = function (application, type, user, data, session, callback) {
	this.collection.insert({
		'application' : application,
		'type' : type,
		'user' : user,
		'data' : data,
		'date' : new Date(),
		'session' : session
	}, callback);
};