'use strict';

var Provider = module.exports = function (db) {
	this.collection = db.collection('codes');
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.get = function (application, code, callback) {
	this.collection.find({ 'application' : application, 'code' : code }).toArray(callback);
};

Provider.prototype.getByApplication = function (application, callback) {
	this.collection.find({ 'application' : application }).toArray(callback);
};

Provider.prototype.exists = function (application, code, callback) {
	this.get(application, code, function (err, array) {
		callback(array.length);
	});
};

Provider.prototype.getMultiple = function (application, codes, callback) {
	this.collection.find({
		'application' : application,
		'code': {$in : codes}
	}).toArray(callback);
};