'use strict';
var _ = require('underscore');
var helper = require('./helper');
var UserProvider = require('./provider-user');
var dateutils = require('date-utils');

var Provider = module.exports = function (db) {
	this.collection = db.collection('sessions');
	this.userprovider = new UserProvider(db);
};

Provider.prototype.find = function (query, callback) {
	this.collection.find(query).toArray(function (err, array) {
		callback(err, array);
	});
};

Provider.prototype.findGroupedByDate = function (query, callback) {
	this.find(query, function (err, array) {
		var grouped = helper.groupByDate(array);
		callback(err, grouped, array);
	});
};

Provider.prototype.get = function (id, callback) {
	this.find({ '_id' : id }, callback);
};

Provider.prototype.getAll = function (callback) {
	this.collection.find().toArray(callback);
};

Provider.prototype.getByType = function (type, callback) {
	this.find({ 'type' : type }, callback);
};

Provider.prototype.add = function (id, ip, type, callback) {
	this.collection.insert({
		'_id' : id,
		'date' : new Date(),
		'ip' : ip,
		'type' : type
	}, callback);
};

Provider.prototype.exists = function (id, callback) {
	this.get(id, function (err, array) {
		callback(array.length > 0);
	});
};

Provider.prototype.groupByDate = function (application, type, callback) {
	this.findGroupedByDate({ 'application' : application, 'type' : type }, function (err, grouped, array) {
		callback(err, grouped);
	});
};

Provider.prototype.groupByDateFiltered = function (application, type, startdate, enddate, user, code, callback) {
	var cbGroup, cbAppend;

	cbGroup = _.bind(function (err, grouped, array) {
		callback(err, grouped);
	}, this);

	cbAppend = _.bind(function (query) {
		this.findGroupedByDate(query, cbGroup);
	}, this);

	this.userprovider.appendFiltersToQuery(
		{ 'application' : application, 'type' : type },
		startdate,
		enddate,
		user,
		code,
		cbAppend
	);
};

Provider.prototype.getCountUniqueAndAveragePerDayFiltered = function (application, type, startdate, enddate, user, code, callback) {
	var count, average, query, cbDistinct, cbGroup, cbAppend;

	cbDistinct = _.bind(function (err, array) {
		callback(err, { 'average' : average, 'count' : count, 'unique' : array.length });
	}, this);

	cbGroup = _.bind(function (err, grouped, array) {
		count = array.length;
		average = (count / _.size(grouped)) || 0;
		this.collection.distinct('ip', query, cbDistinct);
	}, this);

	cbAppend = _.bind(function (q) {
		query = q;
		this.findGroupedByDate(query, cbGroup);
	}, this);

	this.userprovider.appendFiltersToQuery(
		{ 'application' : application, 'type' : type },
		startdate,
		enddate,
		user,
		code,
		cbAppend
	);
};

Provider.prototype.setUser = function (id, user, application, callback) {
	var cbExists = _.bind(function (exists) {
		if (exists) {
			this.collection.update(
				{ '_id' : id },
				{ '$set': { 'user' : user, 'application' : application } },
				callback
			);
		} else {
			callback({ 'success': false, 'error' : 'session with id ' + id + ' does not exist.' }, null);
		}
	}, this);

	this.exists(id, cbExists);
};

Provider.prototype.setAdmin = function (id, admin, application, callback) {
	var cbExists = _.bind(function (exists) {
		if (exists) {
			this.collection.update(
				{ '_id' : id },
				{ '$set': { 'admin' : admin, 'application' : application } },
				callback
			);
		} else {
			callback({ 'success': false, 'error' : 'session with id ' + id + ' does not exist.' }, null);
		}
	}, this);

	this.exists(id, cbExists);
};