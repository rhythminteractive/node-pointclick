'use strict';
var UserProvider = require('./lib/provider-user');
var SessionProvider = require('./lib/provider-session');
var EventProvider = require('./lib/provider-event');
var CodeProvider = require('./lib/provider-code');
var SurveyProvider = require('./lib/provider-survey');
var AdminProvider = require('./lib/provider-admin');
var ApplicationProvider = require('./lib/provider-application');

var PointClick = module.exports = function (db) {
	this.userprovider = new UserProvider(db);
	this.sessionprovider = new SessionProvider(db);
	this.eventprovider = new EventProvider(db);
	this.codeprovider = new CodeProvider(db);
	this.surveyprovider = new SurveyProvider(db);
	this.adminprovider = new AdminProvider(db);
	this.applicationprovider = new ApplicationProvider(db);
	this.helper = require('./lib/helper');
};